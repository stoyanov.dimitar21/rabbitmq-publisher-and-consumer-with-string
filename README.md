To start RabbitMQ with Docker

* docker pull rabbitmq:3.10.5-management
* docker run --rm -it -p 15672:15672 -p 5672:5672 rabbitmq:3.10.5-management
